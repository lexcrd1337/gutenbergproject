import java.io.IOException;
import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class GutenbergProject {
    public static final Log log = LogFactory.getLog(GutenbergProject.class);
    public static final Set<String> STOP_WORDS = new HashSet<>(Arrays.asList("i","me","my","myself","we","our","ours","ourselves","you","your","yours","yourself","yourselves","he","him","his","himself","she","her","hers","herself","it","its","itself","they","them","their","theirs","themselves","what","which","who","whom","this","that","these","those","am","is","are","was","were","be","been","being","have","has","had","having","do","does","did","doing","a","an","the","and","but","if","or","because","as","until","while","of","at","by","for","with","about","against","between","into","through","during","before","after","above","below","to","from","up","down","in","out","on","off","over","under","again","further","then","once","here","there","when","where","why","how","all","any","both","each","few","more","most","other","some","such","no","nor","not","only","own","same","so","than","too","very","s","t","can","will","just","don","should","now"));

    public static class TokenizerMapper extends Mapper<Object, Text, Text, MyMapWritable> {
        private final Text word = new Text();

        public void map(Object lineNumber, Text value, Context context) throws IOException, InterruptedException {
            FileSplit fileSplit = (FileSplit) context.getInputSplit();
            String documentId = fileSplit.getPath().getName();
            StringTokenizer wordTokenizer = new StringTokenizer(value.toString(), "\"',.()?![]#$*-;:_+/\\<>@%& ");

            while (wordTokenizer.hasMoreTokens()) {
                word.set(wordTokenizer.nextToken());
                if (STOP_WORDS.contains(word.toString())) {
                    continue;
                }

                MyMapWritable postings = new MyMapWritable();
                postings.put(new MyObjectWritable(documentId), new MyObjectWritable(String.valueOf(lineNumber)));
                context.write(word, postings);
            }
        }
    }

    public static class LineOccurrencesReducer extends Reducer<Text, MyMapWritable, Text, MyMapWritable> {

        public void reduce(Text word, Iterable<MyMapWritable> values, Context context) throws IOException, InterruptedException {
            Map<String, Set<String>> reducedPostingsMap = new HashMap<>();

            values.forEach(mapWritable -> {
                mapWritable.forEach((key, value) -> {
                    String documentId = ((MyObjectWritable) key).get().toString();
                    String[] lineOccurrences = ((MyObjectWritable) value).get().toString().split(",");

                    if (reducedPostingsMap.containsKey(documentId)) {
                        Set<String> reducedLineOccurrences = reducedPostingsMap.get(documentId);

                        reducedLineOccurrences.addAll(new HashSet<>(Arrays.asList(lineOccurrences)));
                        reducedPostingsMap.put(documentId, reducedLineOccurrences);
                    } else {
                        reducedPostingsMap.put(documentId, new HashSet<>(Arrays.asList(lineOccurrences)));
                    }
                });
            });

            MyMapWritable mapWritable = new MyMapWritable();
            reducedPostingsMap.forEach((documentId, lineOccurrences) ->
                    mapWritable.put(new MyObjectWritable(documentId), new MyObjectWritable(String.join(",", lineOccurrences))));

            context.write(word, mapWritable);
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "Gutenberg Project");
        //Add input and output file paths to job based on the arguments passed
        CustomFileInputFormat.addInputPath(job, new Path(args[0]));
        job.setInputFormatClass(CustomFileInputFormat.class);

        job.setJarByClass(GutenbergProject.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(LineOccurrencesReducer.class);
        job.setReducerClass(LineOccurrencesReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(MyMapWritable.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
