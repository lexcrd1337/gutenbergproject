import org.apache.hadoop.io.ObjectWritable;

public class MyObjectWritable extends ObjectWritable {
    public MyObjectWritable() {
        super();
    }

    public MyObjectWritable(Object object) {
        super(object);
    }

    @Override
    public String toString() {
        return this.get().toString();
    }
}
