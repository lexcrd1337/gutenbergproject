import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Writable;

public class MyMapWritable extends MapWritable {
    public MyMapWritable() {
        super();
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        for (Entry<Writable, Writable> entry : this.entrySet()) {
            result.append("(").append(entry.getKey().toString()).append(", [").append(entry.getValue().toString()).append("])   ");
        }

        return result.toString();
    }
}
